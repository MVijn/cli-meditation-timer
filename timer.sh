#!/usr/bin/env bash

# license Marten Vijn, BSD-style 
# https://github.com/MartenVijn/public/blob/master/svn/LICENSE



if [ $2 ] 
then
  m=$2
else
  echo "use like:"
  echo "./timer.sh <start_ping> <meditation_time> [intermediate gong]"
  echo "./timer.sh 0 10"
  echo "./timer.sh 1 10"
  echo "./timer.sh 0 10 5"
  echo "./timer.sh 1 10 5 " 
  exit 1
 
fi

if  [ -f /usr/bin/aplay ]
then 
  sound_play="/usr/bin/aplay -q"
fi

if  [ -f /usr/bin/afplay ]
then 
  sound_play=/usr/bin/afplay
fi

if [ ! $sound_play ]
then
	echo no sound player found, timing without sound
	sleep 2
fi 

# start ping
if [ $1 = 1 ]
then
	$sound_play ping.wav &
fi


while [ ! $m = 0 ]
do
  ((m=$m-1))
  s=60
  while [ ! $s = 0 ]
  do
	((s=$s - 1))
        clear
	echo $m:$s
	sleep 1
  done
  if [ $3 ] 
  then
    if [ $m = $3 ]
    then 
      $sound_play gong.wav &
    fi
  fi
done

#end gong
$sound_play gong.wav
