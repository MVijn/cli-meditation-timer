
if you have git, open a terminal: 

```
git clone https://gitlab.com/MVijn/cli-meditation-timer.git
cd cli-mediation-timer
chmod +x timer.sh
./timer.sh  for help
```

or download the tarball and open a terminal

```
mkdir ~/cli-timer
cd ~/cli-timer
mv ~/Downloads/timer.tgz . 
tar xvzf timer.tgz
chmod +x  timer.sh
./timer.sh  for help
```


the help says"
```
use like:
./timer.sh <start_ping> <meditation_time> [intermediate gong]
./timer.sh 0 10 
./timer.sh 1 10
./timer.sh 0 10 5
./timer.sh 1 10 5
```

